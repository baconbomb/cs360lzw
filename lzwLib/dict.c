#include <stdlib.h>
#include <dict.h>

/*
 COURSE:        CS 360 Spring 2018
 ASSIGNMENT:    1
 AUTHOR:        Spencer Bacon
 */

Dict* newDict(unsigned int hashSize) {
	Dict* d = malloc(sizeof(Dict));				//allocate memory for Dictionary
	d->size = hashSize;							//specify the size of memory
	d->list = calloc(sizeof(KVpair*), hashSize);	//allocate list of KVpairs in dict
	for(int i=0; i < 256; i++){
		Sequence *s = newSequence(i);		// Create sequence	
		insertIntoDict(d,s, i);				//insert into dictionary
		}
    return d;								//return initiated dictionary
}

KVpair* newKVpair(Sequence* sequence, unsigned int code){
	KVpair* kv = malloc(sizeof(KVpair));		//allocate size of KVpai
	kv->key = sequence;							//new kvpair points at sequence
	kv->code = code;							//store code of sequence
	kv->next = NULL;							//no next kvpair yet, set NULL
	return kv;
}


void deleteDictDeep(Dict* dict) {
					
	for(int i=0; i < dict->size; i++){			//loop through the whole list of kv pairs
		KVpair* kv = dict->list[i];
		while(kv != NULL){
			KVpair* temp = kv->next;		// Get Temp pointer to next kv pair
			deleteSequence(kv->key);		// Delete key for current kv
			free(kv);						// Free current kv
			kv = temp;						//
		}
	}
	free(dict->list);						//free dict pointer to list
	free(dict);								//free dict;
	return;	
}

bool searchDict(Dict* dict, Sequence* key, unsigned int* code) {
	unsigned int c = hash(key)%dict->size;		//hash sequence to find where to store it
	KVpair* kv = dict->list[c];					//kvpair to traverse link list
	while(kv != NULL){							//there is somthing at the hash 'c' location
		if(identicalSequences(kv->key, key)){	//check if the sequences are identical
			*code = kv->code;					//they are, set code* to point at the already
												//stored code;
			return true;
		}
		kv = kv->next;				//not the same, check next in linked list
	}
    return false;				//end of linked list at hash location c, not in dictionary
}

void insertIntoDict(Dict* dict, Sequence* key, unsigned int  code){
	unsigned int hashCode = hash(key)%dict->size;			//hash to store new sequence and code
	
	KVpair *kv = newKVpair(key,code);				//new KVpair at index
	kv->next = dict->list[hashCode];				//append what was there to end of new KVpair
	dict->list[hashCode] = kv;				
	return;
}
	
// Bernstein hash from google / class lecture
unsigned int hash(Sequence* sequence){
	unsigned int h = 0;
	for(int i =0; i<sequence->size; i++){
		h = (h*33) ^ sequence->byte[i];
	}
	return h;							//find location in hash table
}
