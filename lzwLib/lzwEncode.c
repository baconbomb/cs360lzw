#include <sequence.h>
#include <bitStream.h>
#include <dict.h>
#include <lzw.h>
#include <stdio.h>

/*
 COURSE:        CS 360 Spring 2018
 ASSIGNMENT:    1
 AUTHOR:        Spencer Bacon
 */

bool lzwEncode(unsigned int bits, unsigned int maxBits,
               int (*readFunc )(void* context), void* readContext,
               int (*writeFunc)(unsigned char c, void* context), void* writeContext) {
               
	Dict *D = newDict((1<<maxBits)-1);			//use bit shifting for 2^16
	unsigned int nextCode = 256;
//    if(((1 << bits)-1) < nextCode) bits++;
	
	BitStream *BS = openOutputBitStream(writeFunc,writeContext);	//from suedo code
	char C = readFunc(readContext);	
	if(C==-1){
		closeAndDeleteBitStream(BS);
		deleteDictDeep(D);
		return false;
		}
	Sequence *W = newSequence(C);
	unsigned int code;
	while((C=readFunc(readContext)) && C!=-1){
		Sequence *X = copySequenceAppend(W,C);
		if(searchDict(D, X, &code)){
			deleteSequence(W);
			W = X;
			}
		else {
			searchDict(D, W, &code);
			outputBits(BS, bits, code);
			if(nextCode < D->size){
                if(((1 << bits) -1) < nextCode){    //increment bits size of the code
                    bits++;
                    }
				insertIntoDict(D, X, nextCode);
				nextCode++;
				} 
			else {
				deleteSequence(X);
				}
			deleteSequence(W);
			W = newSequence(C);
			}
		}
	searchDict(D, W, &code);			//write out left over
	outputBits(BS, bits, code); 
	deleteSequence(W);						//free memory
	deleteDictDeep(D);
	closeAndDeleteBitStream(BS);
    return true;
}
