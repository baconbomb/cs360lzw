#include <sequence.h>
#include <bitStream.h>
#include <lzw.h>
#include <stdlib.h>

/*
 COURSE:        CS 360 Spring 2018
 ASSIGNMENT:    1
 AUTHOR:        Spencer Bacon
 */

bool lzwDecode(unsigned int bits, unsigned int maxBits,
               int (*readFunc )(void* context), void* readContext,
               int (*writeFunc)(unsigned char c, void* context), void* writeContext) {
               
    unsigned char C;
    unsigned int nextCode, previousCode, currentCode;
    unsigned int maxCode = (1<<maxBits);				//store max number of bits for code
	Sequence** list = calloc(sizeof(Sequence*), maxCode);	//from suedo code
	for(nextCode = 0; nextCode < 256; nextCode++){		//initialize first char of table
		list[nextCode] = newSequence(nextCode);
		}	
	
    BitStream* BS = openInputBitStream(readFunc, readContext);
    if(!readInBits(BS,bits,&previousCode)){
    	return false;			//nothing to read in
    }
    outputSequence(list[previousCode],writeFunc,writeContext);
    if((1 << bits)-1 <= nextCode && bits < maxBits){    //if bits = 8, needs to be incremented now
        bits++;
     }

    while(readInBits(BS, bits, &currentCode)){			//while there are bits to read
    		
       	if(currentCode < nextCode){			//currentCode is within list
   			C = list[currentCode]->byte[0];
   			}
   		else{
    		C = list[previousCode]->byte[0];
    		}
   		if(nextCode < maxCode){
   			Sequence* W = copySequenceAppend(list[previousCode], C);
   			list[nextCode] = W;
            nextCode++;
            if(((1 << bits)-1) < nextCode && bits < maxBits){ //used up all bit space, increment the number of bits used
                bits++;
                }
   			}
    	outputSequence(list[currentCode],writeFunc,writeContext);
    	previousCode = currentCode;
   		}
	closeAndDeleteBitStream(BS);		//free memory
	for(int i=0; i < maxCode; i++){
		deleteSequence(list[i]);
		}
	free(list);
    return true;
}
