#include <stdlib.h>
#include <bitStream.h>

/*
 COURSE:        CS 360 Spring 2018
 ASSIGNMENT:    1
 AUTHOR:        Spencer Bacon
 */

BitStream* openInputBitStream(int (*readFunc)(void* context), void* context) {
	BitStream *bs = malloc(sizeof(BitStream));
	bs->isInput = true;				//is input
	bs->context = context;			//store context
	bs->readFunc = readFunc;		//store function pointer
	bs->writeFunc = NULL;			//not a write stream
    bs->buffer = 0;
    bs->bufferSize = 0;
    return bs;
}

BitStream* openOutputBitStream(int (*writeFunc)(unsigned char c,void* context),void* context) {
    BitStream *bs = malloc(sizeof(BitStream));
	bs->isInput = false;			//is output
	bs->context = context;			//store context
	bs->readFunc = NULL;			//not read
	bs->writeFunc = writeFunc;		//store function pointer
    bs->bufferSize = 0;
    bs->buffer = 0;
    return bs;
}

void closeAndDeleteBitStream(BitStream* bs) {
	if((bs->bufferSize != 0) && (!bs->isInput)){            //left over the needs to be output in buffer
        int byte = bs->buffer << (8 - bs->bufferSize);      //bit shift to move to where its suppose to be in a char
        byte = byte & 0xFF;             //bit mask to only take the important bits
        bs->writeFunc(byte, bs->context);           //output to file
    }
    free(bs);				//free mem
}

void outputBits(BitStream* bs, unsigned int nBits, unsigned int code) {
	if(bs->isInput)		//verify that is output
		return;

    bs->buffer = bs->buffer << nBits;       //move whats in the buffer nBits over to the left
    bs->buffer = bs->buffer | code;         //add new code to end of buffer
	nBits = bs->bufferSize + nBits;      //nBits = old amount in buffer plus new amount coming in

	while(nBits >= 8){		
		nBits -= 8;
		// Shift code over and mask with FF so we can isolate Byte
		int byte = (bs->buffer >> nBits) & 0xFF; 		// 1111 1111, bit mask
		bs->writeFunc(byte,bs->context);		//write out
        bs->buffer = bs->buffer ^ (byte << nBits);  //remove byte we just wrote from buffer
	}
    bs->bufferSize = nBits; 
}

bool readInBits(BitStream* bs, unsigned int nBits, unsigned int* code) {
    if(!bs->isInput)			//is output
        return false;
    unsigned int outputBitSize = nBits;

    while(bs->bufferSize < outputBitSize){          //loop until we have enough bits to write out
        int byte = bs->readFunc(bs->context);       //read in 8 bits
        if(byte == -1){                             //nothing to read in
            return false;
        }
        bs->bufferSize += 8;                        //increment number of bits in buffer
        bs->buffer = (bs->buffer << 8) | byte;      //bit shift over to make room for bits we just read,,, in order
                                                    //add byte to buffer
        }

    bs->bufferSize = bs->bufferSize - outputBitSize;    //buffer size after writing out
    *code = bs->buffer >> bs->bufferSize;               //bit shift to get bits in the right order
    bs->buffer = bs->buffer ^ (*code << bs->bufferSize);    //xor to remove bits we just read into code

    return true;
}
