#include <stdlib.h>
#include <sequence.h>

/*
 COURSE:        CS 360 Spring 2018
 ASSIGNMENT:    1
 AUTHOR:        Spencer Bacon
 */

Sequence* newSequence(unsigned char firstByte) {
	Sequence* s = malloc(sizeof(Sequence));		//create new pointer to sequence
	s->size = 1;								//initialize size
	s->byte = malloc(sizeof(char));				//create pointer to char
    s->byte[0] = firstByte;						//initialize char
    return s;
}

void deleteSequence(Sequence* sequence) {		
	free(sequence->byte);						//free char at array index	
	free(sequence);								//free sequence
	}
	
Sequence* copySequenceAppend(Sequence* sequence, unsigned char addByte) {
    Sequence* s = malloc(sizeof(Sequence));			//create new sequence pointer
    s->size = sequence->size + 1;					//initialize size to +1sequence we are appending
    s->byte = calloc(sizeof(char), s->size);	//allocate memory for size of new sequence
    for(int i=0; i < sequence->size; i++){					//mem copy all sequence to new sequence
    	s->byte[i] = sequence->byte[i];
    	}
    s->byte[s->size-1] = addByte;						//append byte to end
    return s;
}

void outputSequence(Sequence* sequence,
                    int (*writeFunc)(unsigned char c, void* context), void* context) {
	for(int i=0; i<sequence->size; i++){		//loop through whole sequence
		writeFunc(sequence->byte[i],context);	//write char out
	}
}

bool identicalSequences(Sequence* a, Sequence* b) {
	if(a->size != b->size){				//if size isnt the same, cant be the same sequence
		return false;
		}
    for(int i=0; i < a->size; i++){		//loop through each char to see if sequence is the same
    	if(a->byte[i] != b->byte[i]){	//characters arent equal
    		return false;				//sequence arent identical
			}    
		}		
    return true;						//loops all the way through, they are the same sequence
}

