#include <lzw.h>
#include <stdio.h>

/*   Course:    CS 360 Spring 2018
 Assignment:    1
     Author:    Your Name
 */

int myFgetc(void *context){
	return fgetc(context);
}
int myFputc(unsigned char c, void *context){
	return fputc(c,context);
}
int main(int argc, char* argv[]) {
	FILE *in = fopen("in.txt","r");
	FILE *out = fopen("out","w");
	if(lzwEncode(8,16,myFgetc,in,myFputc,out))
		printf("Encode works!\n");
		
	fclose(in); in = NULL;
	fclose(out); out = NULL;
	
	in = fopen("out","r");
	out = fopen("out.txt","w");
	if(lzwDecode(8,16,myFgetc,in,myFputc,out))
		printf("Decode works!\n");
	
	fclose(in);
	fclose(out);
    return 0;
}
